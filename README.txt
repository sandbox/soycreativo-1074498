Module: Current Last.fm song
Author: Soy Creativo <www.soycreativo.mx>


Description
===========
Provides a block to show the current song of a last.fm account.

Requirements
============
Public Key of Last.fm webservice, get it in Last.fm Web Services
http://www.last.fm/api/account

Installation
============
* Copy the 'current_lastfm_song' module directory in to your Drupal
sites/all/modules directory.

Usage
=====
In the settings page enter your Publick Key and the username of last.fm account.

