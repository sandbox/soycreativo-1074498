<?php
/**
* Implementation of hook_admin_settings() for configuring the module
*/
function current_lastfm_song_admin_settings_form(&$form_state){
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => FALSE,
  );
  
  $form['account']['current_lastfm_song_publickey'] = array(
    '#type' => 'textfield',
    '#title' => t('Last.fm public key'),
    '#size' => 40,
    '#default_value' => variable_get('current_lastfm_song_publickey', NULL),
    '#required' => TRUE,
    '#description' => t('The public key can get it in <a href="@url">Last.fm Web Services</a> page.', array('@url' => 'http://www.last.fm/api/account')),
  );
  $form['account']['current_lastfm_song_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Last.fm user'),
    '#size' => 20,
    '#default_value' => variable_get('current_lastfm_song_username', NULL),
    '#required' => TRUE,
    '#description' => t('The username of your last.fm account')
  ); 
  return system_settings_form($form);
}

